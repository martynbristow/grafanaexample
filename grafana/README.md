# Monitoring

`docker-compose -f grafana.yaml up`

## Slack Notification
*https://hooks.slack.com/services/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*

## Queries

`avg(nginx_upstream_response_msecs_avg{upstream=~"$kube_namespace-$ci_environment_slug-.*"})`